#pragma once
#include "MyGameEngine\Node.h"
#include "MyGameEngine\Global.h"
#include <vector>
class Font :public Node
{
	const int	NO_DRAW;			//表示不可定数

	LPD3DXSPRITE _pSprite;			//スプライト
	LPDIRECT3DTEXTURE9 _pTexture;	//テクスチャ
	MyRect     _cut;				//切抜き範囲


	struct COLOR					//色用の構造体作成
	{
		int alpha;					//透明度
		int red;					//赤
		int green;					//緑
		int blue;					//青
	}; COLOR _fontColor;			//計算に使用する色構造体

	bool		_isZero;			//0を表示させるフラグ
	int			_digits;			//自身の桁数
	int			_maxDigits;			//最大桁数
	float		_fontWidth;			//フォントの幅
	LPCSTR		_file;				//使用するファイル
	Font*	_parentFont;			//親フォント
	std::vector<Font*> _pFont;		//動的配列を準備
	Scene* _scene;					//使用するフォント

	//読み込み
	HRESULT load(LPCSTR fileName, MyRect cut = MyRect(-999, -999, -999, -999)); 

	//描画
	HRESULT draw();

	//フォントを生成(桁上げした時に使う)
	static Font* createFont(LPCSTR fileName, MyRect cut = MyRect(-999, -999, -999, -999));

	//桁数取得
	int getDigits(int val);

	//数値を10で割った余りを取得
	int getRemain(int num);

	//親フォントに桁上げフォントを追加
	void addFont(Font* font);	
	
	//親フォントを設定
	void setParentFont(Font* 	parent);

	//桁上げ処理
	void carry(float width, float height, int digits, int set);

	//桁上げ処理(追加で来た値用)
	void check(int val);

	//子フォントの桁数を変更する処理
	void changeCarry();	

	//フォントの幅を設定
	void setFontWidth();

	//桁上げフォント全体の位置を設定
	void setFontPosition(D3DXVECTOR3 pos);

	//フォント全体の大きさを設定
	void setFontScale(D3DXVECTOR3 scale);

	//フォント全体の色を設定
	void setFontColor(struct COLOR color);

	//設定したフォントの色を取得
	Font::COLOR getColor();

	//0を表示
	void setZeroDraw();

	//ワールド座標を取得
	D3DXMATRIX getWorldMatrix();

public:
	//コンストラクタ
	Font();		

	//デストラクタ
	~Font();	

	//機能:フォントを作成(最初にフォントを生成する時に使う)
	//引数:ファイル名(LPCSTR fileName), 切り取り幅(float width), 切り取り高さ(float height), 初期値(int val), 使用するシーン(Scene* scene)
	//戻値:Font型ポインタ(Font*)
	static Font* createFont(LPCSTR fileName, float width, float height, int val, Scene* scene);

	//機能:数値を設定
	//引数:使用したい数値(int number)
	//戻値:なし
	void setNumber(int number);

	//機能:フォントの幅を設定
	//引数:フォントの幅(float width)
	//戻値:なし
	void setWidth(float width);

	//機能:フォントの透明度と色を設定
	//引数:透明度(int alpha),赤色(int red), 緑色(int green), 青色(int blue)
	//戻値:なし
	void setColor(int alpha, int red, int green, int blue);

	//機能:フォントの色を設定
	//引数:赤色(int red), 緑色(int green), 青色(int blue)
	//戻値:なし
	void setColor(int red, int green, int blue);

	//機能:0を表示させる
	//引数:なし
	//戻値:なし
	void zeroDraw();
};
