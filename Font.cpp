#include "Font.h"
#include "MyGameEngine\Scene.h"
#include <math.h>

//機能:コンストラクタ
//引数:なし
//戻値:なし
Font::Font()
:NO_DRAW(9999)
{
	//_pSpriteを初期化
	_pSprite = nullptr;	

	//_pTextureを初期化
	_pTexture = nullptr;	

	//色の初期化用構造体defColor作成
	struct COLOR defColor = { 255, 255, 255, 255 };

	//初期状態では0を表示させない
	_isZero = false;

	//色情報を初期化
	_fontColor = defColor;

	//自身の桁数を初期化
	_digits = 0;

	//フォントの幅を初期化
	//_fontWidth = -18;
	_fontWidth = 0;

	//親フォントを初期化
	_parentFont = nullptr;
}

//機能:デストラクタ
//引数:なし
//戻値:なし
Font::~Font()
{
	SAFE_RELEASE(_pTexture);	//_pTextureの開放
	SAFE_RELEASE(_pSprite);		//_pSpriteの開放
}

//============================================================================================
//
//	ここから、private(ユーザーでは処理が呼べない)
//
//============================================================================================

//機能:スプライト・テクスチャ作成
//引数:fileName 画像ファイル名
//戻値:なし
HRESULT Font::load(LPCSTR fileName, MyRect cut)
{
	//スプライトオブジェクト作成
	//エラーチェック(スプライト作成)
	if (FAILED(D3DXCreateSprite(g.pDevice, &_pSprite)))
	{
		//エラーが出たら表示
		MessageBox(NULL, "スプライト作成に失敗", "エラー", MB_OK);

		//E_FAILを返す
		return E_FAIL;													
	}
	//テクスチャオブジェクトの作成
	//エラーチェック(テクスチャ作成)
	if (FAILED(D3DXCreateTextureFromFileEx(g.pDevice, fileName, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture)))
	{
		//エラーが出たら表示
		MessageBox(NULL, "テクスチャ作成に失敗", fileName, MB_OK);	

		//E_FAILを返す
		return E_FAIL;													
	}
	//全体表示
	if (cut._left == -999)
	{
		//テクスチャのサイズを取得
		D3DSURFACE_DESC d3dds;

		//_pTextureのテクスチャのサイズを取得
		_pTexture->GetLevelDesc(0, &d3dds);	

		//_size.xにd3dds.Width(幅)を代入
		_size.x = d3dds.Width;

		//_size.yにd3dds.Height(高さ)を代入
		_size.y = d3dds.Height;						

		//切抜き範囲
		_cut = MyRect(0, 0, _size.x, _size.y);

	}

	//切抜き範囲指定
	else
	{
		//切抜き範囲
		_cut = cut;				//_cutにcutの値を代入
		_size.x = _cut._width;	//_size.xに_cut._widthを代入
		_size.y = _cut._height;	//_size.yに_cut._heightを代入
	}
	return S_OK;				//S_OKを返す
}

//機能:スプライトを描画する
//引数:なし
//戻値:なし
HRESULT Font::draw()
{
	//D3DXMATRIX型のworld構造体を作成
	D3DXMATRIX world;

	//タグがFont(桁上げフォント)だったら
	if (_tag == "Font")
	{
		//表示する大きさを設定する
		setFontScale(_parentFont->getScale());

		//フォント全体の色を設定
		setFontColor(_parentFont->getColor());

		//表示する座標を設定する
		setFontPosition(_parentFont->getPosition());

		//最大桁の0を表示するかどうか
		setZeroDraw();
		
		//ワールド座標を作成する
		world = this->getWorldMatrix();
	}
	else
		//ワールド座標を作成する
		world = this->getWorldMatrix();

	//変換行列をセット
	_pSprite->SetTransform(&world);

	//D3DXVECTOR3型のanchor構造体に計算で求めたベクトルを代入
	D3DXVECTOR3 anchor = D3DXVECTOR3(_size.x * _anchorPoint.x, _size.y *(1.0f - _anchorPoint.y), 0);

	//切抜き範囲
	RECT cut = {					//RECT型のcut構造体を作成
		_cut._left,					//左端を求める
		_cut._top,					//上部を求める
		_cut._left + _cut._width,	//右端を求める
		_cut._top + _cut._height	//下部を求める
	};

	//スプライトの描画の準備をする
	_pSprite->Begin(D3DXSPRITE_ALPHABLEND);

	//エラーチェック(描画処理)
	if (FAILED(_pSprite->Draw(_pTexture, &cut, &anchor, nullptr, D3DCOLOR_ARGB(_fontColor.alpha,_fontColor.red,_fontColor.green,_fontColor.blue))))
	{
		//エラーが出たら表示
		MessageBox(NULL, "スプライトの描画に失敗", "エラー", MB_OK);
		//E_FAILを返す
		return E_FAIL;													
	}

	//スプライトの描画を終わる
	_pSprite->End();

	//S_OKを返す
	return S_OK;	
}

//機能:フォントを生成(桁上げした時に使う)
//引数:ファイル名(LPCSTR fileName), 切り取り範囲(MyRect cut)
//戻値:Font型ポインタ(Font* pFont)
Font* Font::createFont(LPCSTR fileName, MyRect cut)
{
	//pFontクラスのインスタンス(実体)を作る
	Font* pFont = new Font();		

	//フォントを作成
	pFont->load(fileName, cut);				

	//Font型ポインタを返す
	return pFont;
}

//機能:桁数取得
//引数:入力された数値(int val)
//戻値:桁数(int ans)
int Font::getDigits(int val)
{
	//戻り値用の変数作成
	int digits = 0;

	//入力された数値が0以下だったら
	if (val <= 0)
		//入力された数値を0にする
		return 0;

	//桁数を計算して戻り値用の変数に代入
	digits = (int)log10((double)val);

	//桁数を返す
	return digits;
}

//機能:数値を10で割った余りを取得
//引数:入力された数値(int num)
//戻値:余り(int reset)
int Font::getRemain(int num)
{
	//戻り値用の変数作成
	int rem = 0;

	//数値を10で割った余りを戻り値用の変数に代入
	rem = num % 10;

	//余りを返す
	return rem;
}

//機能:親フォントに桁上げフォントを追加
//引数:桁上げフォント(Font* font)
//戻値:なし
void Font::addFont(Font* font)
{
	//桁上げフォントの親を設定する
	font->setParentFont(this);

	//桁上げフォントを登録
	_pFont.push_back(font);
}

//機能:親フォントを設定
//引数:登録したいフォント(Font* font)
//戻値:なし
void Font::setParentFont(Font* 	parent)
{
	//親フォントに登録したいフォントを代入する
	_parentFont = parent;
}

//機能:桁上げ処理
//引数:切り取り幅(float width), 切り取り高さ(float height), 桁数(int digits), 数値(int set)
//戻値:なし
void Font::carry(float width, float height, int digits, int set)
{
	//切り取り範囲を設定
	MyRect cut = MyRect(width * set, 0, width, height);

	//桁上げフォントを作成
	Font* pFontN = createFont(_file, cut);

	//桁上げフォントのタグを設定
	pFontN->setTag("Font");

	//桁数を設定
	pFontN->_digits = digits;

	//親フォントに追加
	this->addFont(pFontN);

	//表示するシーンに登録
	this->_scene->addChild(pFontN);
}

//機能:桁上げ判断
//引数:数値(int val)
//戻値:なし
void Font::check(int val)
{
	//桁チェック用の変数作成
	int check = getDigits(val);

	//最大桁数が桁チェック用の変数より大きくなるまで繰り返す
	while (_maxDigits < check)
	{
		//最大桁数に1追加する
		_maxDigits += 1;

		//表示する数値を作成
		int set = val / pow(10, check);

		//桁上げ処理を呼ぶ
		carry(_cut._width, _cut._height, _maxDigits, set);
	}
}

//機能:桁上げフォントの桁数を変更する処理
//引数:なし
//戻値:なし
void Font::changeCarry()
{
	//チェック用変数作成
	static int digits = 1;
	//最大桁数がチェック用変数より大きいとき
	if (_maxDigits > digits)
	{
		//桁上げフォント分繰り返す
		for (int i = 0; i < _pFont.size(); i++)
		{
			//桁上げフォントから1引いた分繰り返す
			for (int j = _pFont.size() - 1; j > i; j--)
			{
				//自分の前の桁が、自分より大きかったら
				if (_pFont[j]->_digits > _pFont[j - 1]->_digits)
				{
					//自分の前の桁を一時的に記録する変数に格納
					int work = _pFont[j - 1]->_digits;

					//自分の前の桁に自分の桁数を設定
					_pFont[j - 1]->_digits = _pFont[j]->_digits;

					//自分の桁に前の桁を設定
					_pFont[j]->_digits = work;
				}
			}
		}
		//チェック用変数を更新
		digits = _maxDigits;
	}
}



//機能:フォント全体の幅を設定
//引数:なし
//戻値:なし
void Font::setFontWidth()
{
	//子フォント全体に入力した幅を代入させる
	for (int i = 0; i < _pFont.size(); i++)
	{
		//幅を記録させる
		_pFont[i]->_fontWidth = _fontWidth;
	}
}

//機能:桁上げフォント全体の位置を設定
//引数:フォントの幅(D3DXVECTOR3 pos)
//戻値:なし
void Font::setFontPosition(D3DXVECTOR3 pos)
{
	//自身の位置設定を呼ぶ(Nodeクラス)
	this->setPosition(((_fontWidth * this->_digits)) * _scale.x,0,0);

	//親フォントの位置と連動させる
	this->setPosition(_position.x + pos.x, pos.y, pos.z);
}

//機能:フォント全体の大きさを設定
//引数:フォントの幅(D3DXVECTOR3 scale)
//戻値:なし
void Font::setFontScale(D3DXVECTOR3 scale)
{
	//自身の大きさ設定を呼ぶ(Nodeクラス)
	this->setScale(scale.x, scale.y, scale.z);
}

//機能:フォント全体の色を設定
//引数:フォントの色(struct COLOR color)
//戻値:なし
void Font::setFontColor(struct COLOR color)
{
	//自身の色設定を呼ぶ(Fontクラス)
	this->setColor(color.alpha, color.red, color.green, color.blue);
}

//機能:設定したフォントの色を取得
//引数:なし
//戻値:フォントの色(struct COLOR color)
Font::COLOR Font::getColor()
{
	//自身の色を返す
	return _fontColor;
}

//機能:0を表示
//引数:なし
//戻値:なし
void Font::setZeroDraw()
{
	//「0」を表示する場合
	if (!(this->_parentFont->_isZero))
	{
		//自分が最大桁数だったら
		if (this->_digits == this->_parentFont->_maxDigits)
		{
			//表示する値が0だったら
			if (this->_cut._left == 0)
				//表示させない
				this->_cut._left = NO_DRAW;
		}
	}
}

//機能:ワールド座標を取得する
//引数:なし
//戻値:ワールド座標(D3DXMATRIX world)
D3DXMATRIX Font::getWorldMatrix()
{
	//移動行列(cocos2d-x風) 
	//D3DXMATRIX型のtrans構造体を作成
	D3DXMATRIX trans;	

	//transに移動値を格納
	D3DXMatrixTranslation(&trans, _position.x, (g.WINDOW_HEIGHT - _position.y), 0);

	//拡大縮小
	//D3DXMATRIX型のscal構造体を作成
	D3DXMATRIX scal;

	//scalに拡大率を格納
	D3DXMatrixScaling(&scal, _scale.x, _scale.y, _scale.z);

	//回転行列
	//D3DXMATRIX型のrotate構造体を作成
	D3DXMATRIX rotate;		
								
	//rotateに回転角度を格納
	D3DXMatrixRotationZ(&rotate, D3DXToRadian(0));

	//ワールド行列
	//D3DXMATRIX型のworld構造体を作成し、scal、rotate、transの積を代入
	D3DXMATRIX world = scal * rotate * trans;										

	//ワールド座標を返す
	return world;
}
//============================================================================================
//
//	ここから、public(ユーザーで処理を呼べる)
//
//============================================================================================

//機能:フォントを作成(最初にフォントを生成する時に使う)
//引数:ファイル名(LPCSTR fileName), 切り取り幅(float width), 切り取り高さ(float height), 初期値(int val), 使用するシーン(Scene* scene)
//戻値:Font型ポインタ(Font* pFont)
Font* Font::createFont(LPCSTR fileName, float width, float height, int val, Scene* scene)
{
	//Fontクラスのインスタンス(実体)を作る
	Font* pFont = new Font();

	//Fontで使用するファイルを登録(桁上げ時に利用)
	pFont->_file = fileName;

	//Fontで使用するシーンを登録(桁上げ時に利用)
	pFont->_scene = scene;

	//現在の桁数を取得
	int digits = pFont->getDigits(val);

	//最大桁数に現在の桁数を保存
	pFont->_maxDigits = digits;

	//現在の数値を計算用最大数に保存
	int max = val;

	//桁数分ループ
	for (int i = digits; i > 0; i--)
	{
		//切り取り用変数作成
		int set = max / pow(10, i);

		//桁上げ処理を呼ぶ
		pFont->carry(width, height, i, set);

		//計算用最大値を更新する
		max -= set * pow(10, i);
	}

	//一桁目の切り取り範囲を設定
	MyRect cut = MyRect(width * pFont->getRemain(val), 0, width, height);

	//フォントを作成
	pFont->load(fileName, cut);

	//基本幅を設定
	pFont->_fontWidth = -(width / 2 + 2);

	//Font型ポインタを返す
	return pFont;
}

//機能:数値を設定
//引数:使用したい数値(int number)
//戻値:なし
void Font::setNumber(int number)
{
	//入力された数値が0以下だったら
	if (number<0)
		//入力された数値を0にする
		number = 0;

	//入力された数値を計算用最大数に保存
	int max = number;

	//桁上げ判断処理を呼ぶ
	check(number);

	//桁数の変更処理を呼ぶ
	changeCarry();

	//桁幅を登録させる
	setFontWidth();

	//子フォント分繰り返す
	for (int i = _pFont.size(); i > 0; i--)
	{
		//切り取り用変数作成
		int set = max / pow(10, i);

		//表示する数値を変更
		_pFont[_pFont.size() - i]->_cut._left = 32 * set;

		//計算用最大値を更新する
		max -= set * pow(10, i);
	}

	//入力された数値の1の位を取得
	int s = getRemain(number);

	//取得した数値を設定する
	_cut._left = s * 32;
}

//機能:フォントの幅を設定
//引数:フォントの幅(float width)
//戻値:なし
void Font::setWidth(float width)
{
	//フォントの幅に入力した値を設定
	_fontWidth += -width;
}

//機能:フォントの透明度と色を設定
//引数:透明度(int alpha),赤色(int red), 緑色(int green), 青色(int blue)
//戻値:なし
void Font::setColor(int alpha, int red, int green, int blue)
{
	//フォントの透明度に入力した数値を設定
	_fontColor.alpha = alpha;

	//色の設定処理を呼ぶ
	setColor(red,green,blue);
}

//機能:フォントの色を設定
//引数:赤色(int red), 緑色(int green), 青色(int blue)
//戻値:なし
void Font::setColor(int red, int green, int blue)
{
	//赤色に入力した値を設定
	_fontColor.red = red;

	//緑色に入力した値を設定
	_fontColor.green = green;

	//青色に入力した値を設定
	_fontColor.blue = blue;
}

//機能:0を表示させる
//引数:なし
//戻値:なし
void Font::zeroDraw()
{
	//0を表示させるフラグをtrueにする
	_isZero = true;
}
